jQuery(document).ready(function(){
	var width = $(document).width();
	if(width<960) {
		$('aside h3').on('click',function(){
			$('aside p').hide();
			$('aside h3 span').hide();
			$('aside h3').removeClass('active');
			$(this).find('span').show();
			$(this).next().show();
			$(this).addClass('active');
		});		
	}
	if(width<600){
		$('menu .menu-list li').removeClass('active')
		$('menu').on('click', function(){
			$('menu .menu-list').show();
			$('aside .tag-cloud').hide();
			$('aside p').hide();
			$('aside h3 span').hide();
			$('aside .tag-cloud').hide();		    
		});
		$('aside .tag-cloud-wrapper').on('click', function(){
			$('aside .tag-cloud').show();
			$('menu .menu-list').hide();
			$('aside p').hide();
			$('aside h3 span').hide();
		});
		$('aside h3').on('click',function(){
			$('aside p').hide();
			$('aside h3 span').hide();
			$('aside .tag-cloud').hide();
			$('aside h3').removeClass('active');
			$(this).find('span').show();
			$(this).next().show();
			$(this).addClass('active');
		});	
	}

	$(document).scroll(function(){
		var scroll = $(window).scrollTop();
		if(width>600){		
			if(scroll>87) {
				$('body').addClass('fixed');
				if(width>960) {			
					$('div.small-logo').fadeIn(1000);
				}

			}
			else {
				$('body').removeClass('fixed');
				$('div.small-logo').fadeOut(1000);
			}
		}else {
			if(scroll>60) {
				$('body').addClass('fixed');

			}
			else {
				$('body').removeClass('fixed');
				
			}
		}
	});
});