$(document).ready(function(){
	loadContent();
});


$('#save').on('click', function(){
	var index = $('#tab-index').val(), 
	title = $('#tab-title').val(), 
	content = $('#tab-content').val(),
	form = { 
		Id:index,
		Title:title,
		Content:content,
	},
	ls,
	obj = [form], 
	tabs;
	ls = JSON.parse(localStorage.getItem('tabs'));
	if(ls!== null){ 
		tabs = ls.concat(obj);
	} else {
		tabs = obj;
	}
	localStorage.setItem('tabs', JSON.stringify(tabs));
	loadContent();

});

$('#delete').on('click', function(){	
	
});

$('#close').on('click', function(){
	console.log('closing');
});

$('.title.active').on('click', function(){
	console.log('closing');
});

$('.collapse-button').on('click', function(){
	var aside_left = parseInt($('aside').css('left'));
	var hide;
	var show;
	if(aside_left<0){
		var left = parseInt($('aside').css('left'));
		var sec = parseInt($('section').css('width'));
		$('aside').css('left', 0);
		$('section').css('width',sec-320);
	} 
	if(aside_left===0) {
		var left = parseInt($('aside').css('left'));
		var sec = parseInt($('section').css('width'));
		$('aside').css('left', left-320);
		$('section').css('width',sec+320);
	}
});

function loadContent(){
	var xhr = new XMLHttpRequest(), data;
	xhr.open( 'GET', '../data/default.json', true );

	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;

		if (this.status != 200) {
			console.log( 'Error ' + xhr.status + ': ' + xhr.statusText );
			return;
		}
		var from_server = JSON.parse( xhr.responseText )['Tabs'],
		from_ls = JSON.parse(localStorage.getItem('tabs')),
		data;
		if(from_ls !==null){
			data = from_server.concat(JSON.parse(localStorage.getItem('tabs')));
		} else {
			data = from_server;
		}
		createTab( data );   
	}
	xhr.send();
}

function createTab ( data ) {
	var output 	= '<div class="content-wrapper"><ul class="tab-names">';
	for ( var i = 0; i < data.length; i++) {
		if(i!==0){
		output += '<li id="'+ data[i]['Id'] + '"><div class="title"></div><h4>' + data[i]['Title'] + '</h4>';
	} else {
		output += '<li id="'+ data[i]['Id'] + '"><h4>' + data[i]['Title'] + '</h4><div class="title active"></div>';
	}

	}

	output += '</ul>';

	for ( var i = 0;  i < data.length; i++ ) {
		if(i!==0){
			output += '<article class="hidden"><div class="content"><span>' + data[i]['Content'] + '</span>';
		} else {
			output += '<article id="'+ data[i]['Id'] + '"><div class="content"><span>' + data[i]['Content'] + '</span>';
		}
		output += '</div></article>';
	}

	output += '</div>';
	$('div.content-wrapper').replaceWith(output);
};

